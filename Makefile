PACKAGE=adopt-openjdk-8-jdk
VERSION=0.1
OBJ=$(PACKAGE)_$(VERSION)_all.deb

install : jdk $(OBJ)
	apt-get install ./$(OBJ)

$(OBJ) : build

build : $(PACAKGE)
	which equivs-build || apt-get install -y equivs
	equivs-build adopt-openjdk-8-jdk

all : install README

install : jdk $(OBJ)
	apt-get install ./$(OBJ)

jdk :
	apt-get install -y software-properties-common wget 
	wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
	add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
	apt update

all : install README

README : $(PACKAGE)
	cat $(PACKAGE) | egrep -A100 '^Description: ' | cut -f 2- -d ' ' > README
	
clean : 
	rm $(OBJ) || true

.PHONY: build all install jdk clean


